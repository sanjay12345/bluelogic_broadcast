import React from "react";
import logo from "./../public/images/Login/BL-Logo.svg";
import loaderForgot from "../public/images/loaderfwd.gif";
import listUser from "../public/images/user-icon.svg";
import logoTop from "../public/images/logo.svg";
import icLogout from "../public/images/ic_logout.svg";
import icUser from "../public/images/ic_user.svg";
import icDashboard from "../public/images/ic_dashboard.svg";
import icMessage from "../public/images/ic_message.svg";
import icDocuments from "../public/images/ic_documents.svg";
// import PieChart from "react-minimal-pie-chart";

import "./Subscriber.scss";
import { Link } from "react-router-dom";
import _ from "lodash";
import $ from "jquery";
import ReactApexChart from "react-apexcharts";
import swal from "sweetalert";

// import LineChart from "react-linechart";
// import "../node_modules/react-linechart/dist/styles.css";

class Subscriber extends React.Component {
  constructor(props) {
    super(props);
    this.allLasers = this.allLasers.bind(this);
    this.activateLasers = this.activateLasers.bind(this);
    this.nonactivateLasers = this.nonactivateLasers.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.state = {
      activerecord: [],
      allrecord: [],
      nonactiverecord: [],
      fields: [],
      selectList: [],
      selectAllList: [],
      messageselectList: [],
      subscriberCount: "",
      subscriberActive: "",
      totalMsg: [],
      totalMsgMonth: [],
      msgToday: "",
      all: [],
      series: [0, 0],
      options: {
        chart: {
          width: 380,
          type: "pie"
        },
        labels: ["Total Message", "Today Mesaage"],
        responsive: [
          {
            breakpoint: 480,
            options: {
              chart: {
                width: 200
              },
              legend: {
                position: "bottom"
              }
            }
          }
        ]
      },
      mseries: ["Total Subscribers", "Today Subscribers"],
      moptions: {
        chart: {
          width: 380,
          type: "pie"
        },
        labels: ["Total Subscribers", "Today Subscribers"],
        responsive: [
          {
            breakpoint: 480,
            options: {
              chart: {
                width: 200
              },
              legend: {
                position: "bottom"
              }
            }
          }
        ]
      },
      sseries: [
        {
          name: "Messages",
          data: [35, 35]
        }
      ],
      soptions: {
        chart: {
          height: 350,
          type: "line",
          zoom: {
            enabled: false
          }
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          curve: "straight"
        },
        title: {
          text: "Message Trends by Month",
          align: "left"
        },
        grid: {
          row: {
            colors: ["#f3f3f3", "transparent"], // takes an array which will be repeated on columns
            opacity: 0.5
          }
        },
        xaxis: {
          categories: ["DEC", "JAN"]
        }
      },
      zseries: [
        {
          name: "Subscribers",
          data: [2, 5, 11, 2]
        }
      ],
      zoptions: {
        chart: {
          height: 350,
          type: "line",
          zoom: {
            enabled: false
          }
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          curve: "straight"
        },
        title: {
          text: "Subscribers Trends by Month",
          align: "left"
        },
        grid: {
          row: {
            colors: ["#f3f3f3", "transparent"], // takes an array which will be repeated on columns
            opacity: 0.5
          }
        },
        xaxis: {
          categories: ["Aug", "Sept", "Nov", "Dec"]
        }
      }
    };
  }
  componentDidMount() {
    $(document).ready(function () {
      $("#ckbCheckAll").click(function () {
        $(".checkBoxClass").prop('checked', $(this).prop('checked'));
      });

      $(".checkBoxClass").change(function () {
        if (!$(this).prop("checked")) {
          $("#ckbCheckAll").prop("checked", false);
        }
      });
    });
    this.allLasers();
    this.subscriberCount();
    this.subscriberActive();
    this.msgToday();
    this.totalMsg();
    this.activateLasers();
    setTimeout(() => {
      this.piechart();
    }, 3000);
    // this.piechart();
  }
  componentDidUpdate() {
    // console.log("Inside");
    $("#sidebar-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  }
//   alertmessage() {
//     console.log("inside");
//     return (swal("Hello world!");
// )
//   }
  onSubmit(e) {
    var token = localStorage.getItem("Token");
    var tokenvalue = "Token " + token;
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: tokenvalue
      }
    };
    let Message = this.state.text;
    console.log(this.state.selectAllList);
    let Mobile_Numbers = this.state.selectList;
    let All_Mobile_Numbers = this.state.selectAllList;
    console.log(Mobile_Numbers.length);
    console.log(All_Mobile_Numbers.length);
    let url =
      "http://ec2-13-59-121-213.us-east-2.compute.amazonaws.com/api/groupmessage/";
    let murl =
      "http://ec2-13-59-121-213.us-east-2.compute.amazonaws.com/api/message/";
    const requestOptions = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: tokenvalue
      },
      body: JSON.stringify({ Message, Mobile_Numbers })
    };
    const mrequestOptions = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: tokenvalue
      },
      body: JSON.stringify({ Message })
    };
    console.log(this.state.selectAllList);
    if (Message != undefined) {
      if (Mobile_Numbers.length != 0) {
        return fetch(`${url}`, requestOptions)
          .then(res => res.json())
          .then(res => {
            console.log(res);
            if (res.message != "success") {
              console.log("isnide");
              swal(res.message);
              this.setState({
                Validation: "Only admin can send messages using this API"
              });
            } else {
              this.props.history.push("/Subscriber");
            }

            return Message;
          });
      }
      else if (All_Mobile_Numbers.length != 0) {
        return fetch(`${murl}`, mrequestOptions)
          .then(res => res.json())
          .then(res => {
            console.log(res);
            if (res.message != "success") {
              console.log("isnide");
              swal(res.message);
              this.setState({
                Validation: "Only admin can send messages using this API"
              });
            } else {
              this.props.history.push("/Subscriber");
            }

            return Message;
          });
           } else {
             // alert("Please Select a Contact First");
             swal("Please Select a Contact First or Select All to Sent all");
           }
    } else {
      swal("Please Type a Messge");
    }
  }

  allLasers() {
    var token = localStorage.getItem("Token");
    var tokenvalue = "Token " + token;
    const config = {
      headers: { Authorization: tokenvalue }
    };
    let url =
      "http://ec2-13-59-121-213.us-east-2.compute.amazonaws.com/api/subscriber/all/";
    return fetch(`${url}`, config)
      .then(res => res.json())
      .then(res => {
        this.setState({
          fields: res
        });
        return res;
      });
  }
  totalMsg() {
    var token = localStorage.getItem("Token");
    var tokenvalue = "Token " + token;
    const config = {
      headers: { Authorization: tokenvalue }
    };
    let url =
      "http://ec2-13-59-121-213.us-east-2.compute.amazonaws.com/api/analytics/totalMsg";
    return fetch(`${url}`, config)
      .then(res => res.json())
      .then(res => {
        console.log(res);
        res.map(all => this.state.totalMsg.push(all.count));
        res.map(all => this.state.totalMsgMonth.push(all.month));
        this.setState(
          {
            totalMsg: this.state.totalMsg,
            totalMsgMonth: this.state.totalMsgMonth
          },
          console.log(this.state.totalMsgMonth)
        );
        return res;
      });
  }

  msgToday() {
    var token = localStorage.getItem("Token");
    var tokenvalue = "Token " + token;
    const config = {
      headers: { Authorization: tokenvalue }
    };
    let url =
      "http://ec2-13-59-121-213.us-east-2.compute.amazonaws.com/api/analytics/msgToday";
    return fetch(`${url}`, config)
      .then(res => res.json())
      .then(res => {
        console.log(res);
        this.setState({
          msgToday: res
        });
        return res;
      });
  }
  subscriberCount() {
    var token = localStorage.getItem("Token");
    var tokenvalue = "Token " + token;
    const config = {
      headers: { Authorization: tokenvalue }
    };
    let url =
      "http://ec2-13-59-121-213.us-east-2.compute.amazonaws.com/api/analytics/subscriberCount";
    return fetch(`${url}`, config)
      .then(res => res.json())
      .then(res => {
        console.log(res);
        this.setState({
          subscriberCount: res
        });
        return res;
      });
  }
  subscriberActive() {
    var token = localStorage.getItem("Token");
    var tokenvalue = "Token " + token;
    const config = {
      headers: { Authorization: tokenvalue }
    };
    let url =
      "http://ec2-13-59-121-213.us-east-2.compute.amazonaws.com/api/analytics/subscriberActive";
    return fetch(`${url}`, config)
      .then(res => res.json())
      .then(res => {
        this.setState({
          subscriberActive: res
        });
        return res;
      });
  }
  activateLasers() {
    var token = localStorage.getItem("Token");
    var tokenvalue = "Token " + token;
    const config = {
      headers: { Authorization: tokenvalue }
    };
    let url =
      "http://ec2-13-59-121-213.us-east-2.compute.amazonaws.com/api/subscriber/active/";
    return fetch(`${url}`, config)
      .then(res => res.json())
      
      .then(res => {
        res.map(all => this.state.allrecord.push(all.user_number));
        this.setState({
          activerecord: res
        });
        return res;
      });
      
  }
  nonactivateLasers() {
    var token = localStorage.getItem("Token");
    var tokenvalue = "Token " + token;
    const config = {
      headers: { Authorization: tokenvalue }
    };
    let url =
      "http://ec2-13-59-121-213.us-east-2.compute.amazonaws.com/api/subscriber/not-active/";
    return fetch(`${url}`, config)
      .then(res => res.json())
      .then(res => {
        console.log(res);
        this.setState({
          nonactiverecord: res
        });
        return res;
      });
  }
  openNav() {
    document.getElementById("mySidebar").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
  }
  closeNav() {
    document.getElementById("mySidebar").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
  }

  handleClick(type, user_number) {
    this.setState({ messageselectList: user_number });
    
    console.log(user_number);
    if (type === "active") {
      let selectList = this.state.selectList;
      let index = selectList.indexOf(user_number);
      if (index === -1) {
        selectList.push(user_number);
      } else {
        selectList.splice(index, 1);
      }
      this.setState({ selectList: selectList });
      console.log(this.state.selectList);
    }
  }
  handleClick1(type, user_number) {
 
    this.setState({ messageselectList: user_number });
    if (type === "active") {
      let selectAllList = this.state.selectAllList;
      let index = selectAllList.indexOf(user_number);
      if (index === -1) {
        selectAllList.push(user_number);
      } else {
        selectAllList.splice(index, 1);
      }
      this.setState({ selectAllList: selectAllList });
    }
  }
  

   

  logout() {
    localStorage.clear();
  }

  piechart() {
    this.setState({
      series: [
        +this.state.totalMsg[0],
        +this.state.msgToday.message_sent_today
      ],
      // series: [24, 24],
      options: {
        labels: ["Total Message", "Today Mesaage"]
      }
    });
    this.setState({
      mseries: [
        +this.state.subscriberCount.total_subscribers,
        +this.state.subscriberActive.active_subscribers
      ],
      moptions: {
        labels: ["Total Subscribers", "Today Subscribers Active"]
      }
    });
    console.log(this.state.totalMsgMonth);
    console.log(this.state.totalMsg);
    this.setState(
      {
        // sseries: [{ data: [10, 20] }],
        sseries: [{ data: this.state.totalMsg }],

        soptions: {
          xaxis: {
            categories: ["January"]
          }
        }
      },
      () => console.log(this.state.sseries)
    );
  }
  render() {
    
    const fields = _.get(this, "state.fields", []);
    const activerecord = _.get(this, "state.activerecord", []);
    const allrecord = _.get(this, "state.allrecord", []);
    const nonactiverecord = _.get(this, "state.nonactiverecord", []);
    return (
      <div>
        <div className="s-layout">
          <div className="s-layout__sidebar">
            <div
              className="nav flex-column nav-pills"
              id="v-pills-tab"
              role="tablist"
              aria-orientation="vertical"
            >
              <div className="my-3 px-2">
                <img src={logoTop} width="160px" />
              </div>
              <a
                className="nav-link active"
                id="v-pills-dashboard-tab"
                data-toggle="pill"
                href="#v-pills-dashboard"
                role="tab"
                aria-controls="v-pills-dashboard"
                aria-selected="true"
              >
                <span className="">
                  <img src={icDashboard} width="32px" className="pr-2" />
                  Dashboard
                </span>
              </a>

              <a
                className="nav-link"
                id="v-pills-home-tab"
                data-toggle="pill"
                href="#v-pills-home"
                role="tab"
                aria-controls="v-pills-home"
                aria-selected="true"
              >
                <span className="">
                  <img src={icUser} width="32px" className="pr-2" />
                  Users
                </span>
              </a>
              <a
                className="nav-link"
                id="v-pills-messages-tab"
                data-toggle="pill"
                href="#v-pills-messages"
                role="tab"
                aria-controls="v-pills-messages"
                aria-selected="false"
              >
                <span className="">
                  <img src={icMessage} width="32px" className="pr-2" />
                  Messages
                </span>
              </a>
              <a
                className="nav-link"
                id="v-pills-doc-tab"
                data-toggle="pill"
                href="#v-pills-doc"
                role="tab"
                aria-controls="v-pills-doc"
                aria-selected="false"
              >
                <span className="">
                  <img src={icDocuments} width="32px" className="pr-2" />
                  Documentation
                </span>
              </a>
            </div>
          </div>

          <div class="tab-content" id="v-pills-tabContent">
            <div
              className="my-3 px-2 logout"
              onClick={e => {
                this.logout();
              }}
            >
              <Link to={`/`}>
                <img src={icLogout} width="22px" /> Logout
              </Link>
            </div>
            <div
              class="tab-pane fade show active"
              id="v-pills-dashboard"
              role="tabpanel"
              aria-labelledby="v-pills-dashboard-tab"
            >
              <div className="my-3 px-2">
                <img src={logoTop} width="160px" />
              </div>
              <div className="row">
                <div className="col-md-3 col-sm-12 col-12">
                  <div className="box_shaded">
                    Total Subscribers{" "}
                    <span className="box-number">
                      {this.state.subscriberCount.total_subscribers}
                    </span>
                  </div>
                </div>
                <div className="col-md-3 col-sm-12 col-12">
                  <div className="box_shaded">
                    Active Subscribers{" "}
                    <span className="box-number">
                      {this.state.subscriberActive.active_subscribers}
                    </span>
                  </div>
                </div>
                <div className="col-md-3 col-sm-12 col-12">
                  <div className="box_shaded">
                    Total Message{" "}
                    <span className="box-number">{this.state.totalMsg[0]}</span>
                  </div>
                </div>
                <div className="col-md-3 col-sm-12 col-12">
                  <div className="box_shaded">
                    Today Message{" "}
                    <span className="box-number">
                      {this.state.msgToday.message_sent_today}
                    </span>
                  </div>
                </div>

                {/* <PieChart
                  data={[
                    {
                      title: this.state.totalMsg.total_messages,
                      value: this.state.totalMsg.total_messages,
                      color: "#E38627"
                    },
                    {
                      title: this.state.msgToday.message_sent_today,
                      value: this.state.msgToday.message_sent_today,
                      color: "#C13C37"
                    }
                  ]}
                /> */}
              </div>
              <div className="row">
                <div className="col-md-6 col mt-3">
                  <div className="box_shaded_main">
                    <div className="box-title">Message</div>
                    <div id="chart">
                      <ReactApexChart
                        options={this.state.options}
                        series={this.state.series}
                        type="pie"
                        width={450}
                      />
                    </div>
                  </div>
                </div>
                <div className="col-md-6 col mt-3">
                  <div className="box_shaded_main">
                    <div className="box-title">Subscribers</div>
                    <div id="chart">
                      <ReactApexChart
                        options={this.state.moptions}
                        series={this.state.mseries}
                        type="pie"
                        width={500}
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div>
                <div className="App mt-5">
                  {/* <h1>Graph</h1> */}
                  <div className="row">
                    <div id="chart" className="col-6">
                      <div className="box_shaded_main">
                        <ReactApexChart
                          options={this.state.soptions}
                          series={this.state.sseries}
                          type="line"
                          height={350}
                        />
                      </div>
                    </div>
                    <div id="chart" className="col-6">
                      <div className="box_shaded_main">
                        <ReactApexChart
                          options={this.state.zoptions}
                          series={this.state.zseries}
                          type="line"
                          height={350}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div
              class="tab-pane fade"
              id="v-pills-home"
              role="tabpanel"
              aria-labelledby="v-pills-home-tab"
            >
              <main className="">
                <div className="form-main">
                  <div className="my-3 px-2"></div>

                  <ul className="nav nav-tabs">
                    <li className="nav-item active">
                      <a
                        onClick={e => {
                          this.allLasers();
                        }}
                        className="nav-link active"
                        data-toggle="tab"
                        href="#home"
                      >
                        All
                      </a>
                    </li>
                    <li className="nav-item">
                      <a
                        onClick={e => {
                          this.activateLasers();
                        }}
                        className="nav-link"
                        data-toggle="tab"
                        href="#menu2"
                      >
                        Active
                      </a>
                    </li>
                    <li className="nav-item">
                      <a
                        onClick={e => {
                          this.nonactivateLasers();
                        }}
                        className="nav-link"
                        data-toggle="tab"
                        href="#menu1"
                      >
                        Not Active
                      </a>
                    </li>
                  </ul>

                  <div className="tab-content">
                    <div id="home" className="container-fluid tab-pane active">
                      {fields.map(field => {
                        return (
                          <div className="listing_main">
                            <ul>
                              <div className="float-left mr-1">
                                <li>
                                  <img
                                    src={listUser}
                                    className="style"
                                    width="60px"
                                  />
                                </li>
                              </div>
                              <div className="float-left">
                                <li className="cell-number">
                                  {field.user_number}
                                </li>
                                <li className="chat-time">
                                  {field.date_joined}
                                </li>
                              </div>
                            </ul>
                          </div>
                        );
                      })}
                    </div>
                    <div id="menu1" className="container-fluid tab-pane fade">
                      {nonactiverecord.map(field => {
                        console.log(field);
                        return (
                          <div className="listing_main">
                            <ul
                              onClick={e => {
                                this.handleClick();
                              }}
                            >
                              <div className="float-left mr-1">
                                <li>
                                  <img src={listUser} className="style" width="60px" />
                                </li>
                              </div>
                              <div className="float-left">
                                <li className="cell-number">
                                  {field.user_number}
                                </li>
                                <li className="chat-time">
                                  {field.date_joined}
                                </li>
                              </div>
                            </ul>
                          </div>
                        );
                      })}
                    </div>
                    <div id="menu2" className="container-fluid tab-pane fade">
                      {activerecord.map(field => {
                        return (
                          <div>
                            <div className={"listing_main"}>
                              <div className="">
                                <ul>
                                  <div className="float-left mr-1">
                                    <li className="user-circle">
                                      <img
                                         src={listUser}
                                        className="style"
                                        width="60px"
                                      />
                                    </li>
                                  </div>
                                  <div className="float-left">
                                    <li className="cell-number">
                                      {field.user_number}
                                    </li>
                                    <li className="chat-time">
                                      {field.date_joined}
                                    </li>
                                  </div>

                                  {/* {(this.state.selectList == true) ? (
                            <span>Tick</span>
                          ) : (
                              ""
                            )} */}
                                </ul>

                                {/* <button
                                  type="submit"
                                  form="nameform"
                                  value="Submit"
                                  className="float-right"
                                  onClick={e => {
                                    this.onSubmit();
                                  }}
                                >
                                  Submit
                            </button> */}
                              </div>
                            </div>
                          </div>
                        );
                      })}
                    </div>
                  </div>
                </div>
              </main>
            </div>
            <div
              class="tab-pane fade"
              id="v-pills-messages"
              role="tabpanel"
              aria-labelledby="v-pills-messages-tab"
            >
              <div className="row col">
                <div className="col-4">
                  <div class="divc">
                    <input
                      type="checkbox"
                      id="ckbCheckAll"
                      name="sample"
                      onClick={e => {
                        this.handleClick1("active", allrecord);
                      }}
                    ></input>
                    <span className="marginc">Select All </span>
                  </div>
                  {activerecord.map(field => {
                    return (
                      <div>
                        <div className={"listing_main"}>
                          <ul>
                            <div className="float-left mr-1">
                              <li className="user-circle">
                                <input
                                  type="checkbox"
                                  class="checkBoxClass"
                                  id={field.user_number}
                                  onClick={e => {
                                    this.handleClick(
                                      "active",
                                      field.user_number
                                    );
                                  }}
                                ></input>
                                <img
                                  src={listUser}
                                  className="style"
                                  width="60px"
                                />
                              </li>
                            </div>
                            <div className="float-left">
                              <li className="cell-number">
                                {field.user_number}
                              </li>
                              <li className="chat-time">{field.date_joined}</li>
                            </div>

                            {/* {(this.state.selectList == true) ? (
                            <span>Tick</span>
                          ) : (
                              ""
                            )} */}
                          </ul>
                        </div>
                      </div>
                    );
                  })}
                </div>
                <div className="col-8 textc">
                  <textarea
                    rows="10"
                    cols="0"
                    className="w-100 text_area"
                    name="message"
                    placeholder="Type a Message"
                    value={this.state.text}
                    onChange={evt =>
                      this.setState({
                        text: evt.target.value
                      })
                    }
                  ></textarea>

                  <button
                    type="submit"
                    form="nameform"
                    value="Submit button"
                    href="#popup1"
                    className="float-right"
                    onClick={e => {
                      this.onSubmit();
                    }}
                  >
                    Send
                  </button>
                </div>
              </div>
            </div>

            <div
              class="tab-pane fade broadcaster_list"
              id="v-pills-doc"
              role="tabpanel"
              aria-labelledby="v-pills-doc-tab"
            >
              <h2 className="h2-class">How to use WhatsApp Broadcaster</h2>

              <ul className="ul-class">
                <li>Save +14155238886 to contacts.</li>
                <li>
                  Message “join owner-speak” to the number above using whatsapp
                  to join to twilio for whatsapp channel.
                </li>
                <li>
                  Message “subscribe” to subscribe to the whatsapp broadcaster.{" "}
                </li>
                <li>
                  Message “unsubscribe” to unsubscribe from the whatsapp
                  broadcaster.{" "}
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Subscriber;
