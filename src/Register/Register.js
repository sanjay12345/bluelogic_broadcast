import React from "react";
import logo from "./../public/images/Login/BL-Logo.svg";
import loaderForgot from "../public/images/loaderfwd.gif";
import "./Registr.scss";

// import { symbol } from "prop-types";

class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      Username: "",
      Email: "",
      Password: "",
      Validation: "",
      ForgetValidation: "",
      Useremail: "",
      Message: "",
      isloaded1: true
    };
    this.onSubmit = this.onSubmit.bind(this);
  }

  data;
  state = {
    loading: true
  };
  componentDidUpdate() {
    // console.log("Inside");
  }
  componentDidMount() {
    // console.log("Inside");
  }
  onSubmit(e) {
    e.preventDefault();
    // console.log(this.state.Username, this.state.Password);
    const config = {
      headers: { "Content-Type": "application/x-www-form-urlencoded" }
    };
    let email = this.state.Email;
    let username = this.state.Username;
    let password = this.state.Password;
    var token = localStorage.getItem("Token");

    let url =
      "http://ec2-13-59-121-213.us-east-2.compute.amazonaws.com/api/register/";
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ email, username, password })
    };
    return (
      fetch(`${url}`, requestOptions)
        .then(res => res.json())
        .then(res => {
          if (res.message != "success") {
            this.setState({
              Validation: "User already present"
            });
          } else {
            alert("Thanks for Register use Broadcast service");
            this.props.history.push("/");
          }
          //store user details and jwt token in local storage to keep user logged in between page refreshes
          // localStorage.setItem("username", JSON.stringify(this.state.Username));
          return email;
        })
    );
  }


  render() {
    return (
      <div className="login-main">
        <img src={logo} alt="Logo" className="logo" />

        <form onSubmit={this.onSubmit}>
          <span></span>
          <h2 className="text-center sentencecase">Sign Up</h2>
          <div className="form-group">
            <input
              type="text"
              className="form-control"
              id="email"
              placeholder="Username"
              value={this.state.Username}
              onChange={this.handleChange}
              onChange={evt =>
                this.setState({
                  Username: evt.target.value
                })
              }
            />
          </div>
          <div className="form-group">
            <input
              type="text"
              className="form-control"
              id="Email"
              placeholder="Email"
              value={this.state.Email}
              onChange={this.handleChange}
              onChange={evt =>
                this.setState({
                  Email: evt.target.value
                })
              }
            />
          </div>
          <div className="form-group">
            <input
              type="password"
              className="form-control"
              id="Password"
              placeholder="Password"
              value={this.state.Password}
              onChange={evt =>
                this.setState({
                  Password: evt.target.value
                })
              }
            />
            <span className="form_validation">{this.state.Validation}</span>
          </div>
          <div className="form-group form-check"></div>

          <button type="submit" className="btn-login">
            Register
          </button>
        </form>
      </div>
    );
  }
}

export default Register;
