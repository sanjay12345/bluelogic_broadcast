import React from "react";
import {
  BrowserRouter as Router,
  HashRouter,
  Switch,
  Route
} from "react-router-dom";
import "./App.css";

import Login from "./Login/Login";
import Register from "./Register/Register";
import HeaderAfterLogin from "./HeaderAfterLogin/HeaderAfterLogin";
import SidebarMenu from "./SidebarMenu/SidebarMenu";
import Content from "./Content/Content";
import Subscriber from "./Subscriber/Subscriber";
import AuditTable from "./AuditTable/AuditTable";




function App() {
  return (
    <HashRouter>
      <div>
        <Switch>
          <Route exact path="/" component={Login} />
          <Route path="/Register" component={Register} />
          <Route path="/Content" component={Content} />
          <Route path="/Subscriber" component={Subscriber} />

          {/* <Route path="/HeaderAfterLogin" component={HeaderAfterLogin} />
          <Route path="/SidebarMenu" component={SidebarMenu} />
          <Route path="/AuditTable" component={AuditTable} /> */}
        </Switch>
      </div>
    </HashRouter>
  );
}

export default App;
