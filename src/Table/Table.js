import React from "react";
import logo from "./../public/images/Login/BL-Logo.svg";
import loaderForgot from "../public/images/loaderfwd.gif";
import listUser from "../public/images/user-icon.svg";
import logoTop from "../public/images/logo.svg";


import "./Table.scss";
import { Link } from "react-router-dom";
import _ from "lodash";
import $ from "jquery";



class Table extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activerecord: [],
      nonactiverecord: [],
      fields: [],
      selectList: false
    };
    this.allLasers = this.allLasers.bind(this);
    this.activateLasers = this.activateLasers.bind(this);
    this.nonactivateLasers = this.nonactivateLasers.bind(this);
  }
  componentDidUpdate() {
    // console.log("Inside");
    $("#sidebar-toggle").click(function (e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });

  }

  allLasers() {

    var token = localStorage.getItem("Token");
    var tokenvalue = "Token " + token;
    const config = {
      headers: { Authorization: tokenvalue }
    };
    let url =
      "http://ec2-13-59-121-213.us-east-2.compute.amazonaws.com/api/subscriber/all/";
    return fetch(`${url}`, config)
      .then(res => res.json())
      .then(res => {
        this.setState({
          fields: res
        });
        return res;
      });
  }
  activateLasers() {
    var token = localStorage.getItem("Token");
    var tokenvalue = "Token " + token;
    const config = {
      headers: { Authorization: tokenvalue }
    };
    let url =
      "http://ec2-13-59-121-213.us-east-2.compute.amazonaws.com/api/subscriber/active/";
    return fetch(`${url}`, config)
      .then(res => res.json())
      .then(res => {
        this.setState({
          activerecord: res
        });
        return res;
      });
  }
  nonactivateLasers() {
    var token = localStorage.getItem("Token");
    var tokenvalue = "Token " + token;
    const config = {
      headers: { Authorization: tokenvalue }
    };
    let url =
      "http://ec2-13-59-121-213.us-east-2.compute.amazonaws.com/api/subscriber/not-active/";
    return fetch(`${url}`, config)
      .then(res => res.json())
      .then(res => {
        this.setState({
          nonactiverecord: res
        });
        return res;
      });
  }
  openNav() {
    document.getElementById("mySidebar").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
  }
  closeNav() {
    document.getElementById("mySidebar").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
  }


  handleClick(e) {
    this.setState({ selectList: true })
    console.log('The link was clicked.');
  }


  render() {
    const fields = _.get(this, "state.fields", []);
    const activerecord = _.get(this, "state.activerecord", []);
    const nonactiverecord = _.get(this, "state.nonactiverecord", []);
    return (
      <div>
        <div
          class="nav flex-column nav-pills"
          id="v-pills-tab"
          role="tablist"
          aria-orientation="vertical"
        >
          <a
            class="nav-link active"
            id="v-pills-home-tab"
            data-toggle="pill"
            href="#v-pills-home"
            role="tab"
            aria-controls="v-pills-home"
            aria-selected="true"
          >
            Home
          </a>
          <a
            class="nav-link"
            id="v-pills-profile-tab"
            data-toggle="pill"
            href="#v-pills-profile"
            role="tab"
            aria-controls="v-pills-profile"
            aria-selected="false"
          >
            Profile
          </a>
          <a
            class="nav-link"
            id="v-pills-messages-tab"
            data-toggle="pill"
            href="#v-pills-messages"
            role="tab"
            aria-controls="v-pills-messages"
            aria-selected="false"
          >
            Messages
          </a>
          <a
            class="nav-link"
            id="v-pills-settings-tab"
            data-toggle="pill"
            href="#v-pills-settings"
            role="tab"
            aria-controls="v-pills-settings"
            aria-selected="false"
          >
            Settings
          </a>
        </div>
        sas
        <div class="s-layout">
          <div class="s-layout__sidebar">
            <nav class="s-sidebar__nav">
              <ul>
                <li>
                  <span class="s-sidebar__nav-link" href="">
                    <i class="fa fa-home"></i>
                    <em>Subscriber</em>
                  </span>
                </li>
              </ul>
            </nav>
          </div>

          <main class="s-layout__content">
            <div className="form-main">
              <div className="my-3 px-2">
                <img src={logoTop} width="160px" />
              </div>
              <ul className="nav nav-tabs">
                <li className="nav-item">
                  <a
                    onClick={e => {
                      this.allLasers();
                    }}
                    className="nav-link active"
                    data-toggle="tab"
                    href="#home"
                  >
                    Active
                  </a>
                </li>
                <li className="nav-item">
                  <a
                    onClick={e => {
                      this.activateLasers();
                    }}
                    className="nav-link"
                    data-toggle="tab"
                    href="#menu1"
                  >
                    Not Active
                  </a>
                </li>
                <li className="nav-item">
                  <a
                    onClick={e => {
                      this.nonactivateLasers();
                    }}
                    className="nav-link"
                    data-toggle="tab"
                    href="#menu2"
                  >
                    All
                  </a>
                </li>
              </ul>

              <div className="tab-content">
                <div id="home" className="container-fluid tab-pane active">
                  <br />
                  {activerecord.map(field => {
                    return (
                      <div className="listing_main">
                        <ul
                          onClick={e => {
                            this.handleClick();
                          }}
                        >
                          <div className="float-left mr-1">
                            <li>
                              <img src={listUser} width="60px" />
                            </li>
                          </div>
                          <div className="float-left">
                            <li className="cell-number">{field.user_number}</li>
                            <li className="chat-time">{field.date_joined}</li>
                          </div>

                          {/* {(this.state.selectList == true) ? (
                            <span>Tick</span>
                          ) : (
                              ""
                            )} */}
                        </ul>
                      </div>
                    );
                  })}
                </div>
                <div id="menu1" className="container-fluid tab-pane fade">
                  <br />
                  {nonactiverecord.map(field => {
                    return (
                      <div className="listing_main">
                        <ul
                          onClick={e => {
                            this.handleClick();
                          }}
                        >
                          <div className="float-left mr-1">
                            <li>
                              <img src={listUser} width="60px" />
                            </li>
                          </div>
                          <div className="float-left">
                            <li className="cell-number">{field.user_number}</li>
                            <li className="chat-time">{field.date_joined}</li>
                          </div>
                        </ul>
                      </div>
                    );
                  })}
                </div>
                <div id="menu2" className="container-fluid tab-pane fade">
                  <br />

                  {fields.map(field => {
                    return (
                      <div className="listing_main">
                        <ul>
                          <div className="float-left mr-1">
                            <li>
                              <img src={listUser} width="60px" />
                            </li>
                          </div>
                          <div className="float-left">
                            <li className="cell-number">{field.user_number}</li>
                            <li className="chat-time">{field.date_joined}</li>
                          </div>
                        </ul>
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
          </main>

          <div class="tab-content" id="v-pills-tabContent">
            <div
              class="tab-pane fade show active"
              id="v-pills-home"
              role="tabpanel"
              aria-labelledby="v-pills-home-tab"
            ></div>
            <div
              class="tab-pane fade"
              id="v-pills-profile"
              role="tabpanel"
              aria-labelledby="v-pills-profile-tab"
            >
              ...
            </div>
            <div
              class="tab-pane fade"
              id="v-pills-messages"
              role="tabpanel"
              aria-labelledby="v-pills-messages-tab"
            >
              ...
            </div>
            <div
              class="tab-pane fade"
              id="v-pills-settings"
              role="tabpanel"
              aria-labelledby="v-pills-settings-tab"
            >
              ...
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Table;
