import React, { Component } from "react";
import { Link } from "react-router-dom";

import "./HeaderAfterLogin.scss";
import iconSignOut from "./../public/images/HeaderAfterLogin/ic-sign-out.svg";
import logo from "./../public/images/HeaderAfterLogin/logo.svg";
// import logo from "./../public/images/Login/logo.svg";

class HeaderAfterLogin extends Component {
  logout() {
    localStorage.removeItem("user");
    // <Link to="/">Logout</Link>;
  }
  render() {
    // console.log(localStorage.getItem("user"));
    let usertoken = localStorage.getItem("user");
    let username = JSON.parse(localStorage.getItem("username"));

    // console.log(username);
    return (
      <nav className="navbar navbar-expand-lg navbar-dark">
        <span className="navbar-brand">
          {" "}
          <img src={logo} alt="logo" />
        </span>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav ml-auto">
            <li className="nav-item">
              <p>
                <b>Logged in as</b>
                {username}
              </p>
            </li>
            <li className="nav-item">
              <span className="nav-link" onClick={this.logout}>
                <Link to="/" className="logout-text">
                  Sign Out <img src={iconSignOut} />
                </Link>
              </span>
              {/* <span className="nav-link" onClick={this.logout}>
                Sign Out{" "}
                <span>
                  <img src={iconSignOut} />
                </span>
              </span> */}
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}
export default HeaderAfterLogin;
